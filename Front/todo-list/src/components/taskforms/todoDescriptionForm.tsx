import React, {FormEvent, useState} from "react";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {TodoDescription} from "../../redux/models/api/todoDescription";
import {
    useCreateTodoDescriptionAPIMutation,
    useDeleteTodoAPIMutation,
    useDeleteTodoDescriptionAPIMutation,
    useEditTodoAPIMutation,
    useEditTodoDescriptionAPIMutation
} from "../../redux/slices/apiSlice";
import {useDispatch} from "react-redux";
import {addTodoDescription, deleteTodoDescription, editTodoDescription} from "../../redux/slices/todoDescriptionSlice";
import {TodoItem} from "../../redux/models/api/todoItem";
import {deleteTodo, editTodo} from "../../redux/slices/todoSlice";
import {Alert} from "react-bootstrap";

interface TodoDescriptionFormProps {
    todoItem: TodoItem | null;
    todoDescription: TodoDescription | null;
    handleClose: () => void;
}

interface TodoDescriptionFormState {
    dueDate: string;
    priority: number;
    url: string;
    description: string;
}

interface alertState {
    show: boolean;
    error: boolean;
    message: string;
}

export default function TodoDescriptionForm({todoItem, todoDescription, handleClose}: TodoDescriptionFormProps) {
    const [todoDescriptionFormState, setTodoDescriptionFormState] = React.useState<TodoDescriptionFormState>({
        dueDate: todoItem ? new Date(todoItem.dueDate).toISOString() : new Date().toISOString(),
        priority: todoItem ? todoItem.priority : 1,
        url: todoDescription ? todoDescription.url : "",
        description: todoDescription ? todoDescription.description : "",
    });

    const [alertState, setAlertState] = useState<alertState>({
        show: false,
        error: false,
        message: "",
    })


    const dispatch = useDispatch();
    const [createTodoDescriptionAPI] = useCreateTodoDescriptionAPIMutation();
    const [editTodoDescriptionAPI] = useEditTodoDescriptionAPIMutation();
    const [editTodoAPI] = useEditTodoAPIMutation();
    const [deleteTodoAPI] = useDeleteTodoAPIMutation();
    const [deleteTodoDescriptionAPI] = useDeleteTodoDescriptionAPIMutation();


    const onSubmit = async (event: FormEvent) => {
        event.preventDefault();
        if (todoDescriptionFormState.url !== todoDescription?.url || todoDescriptionFormState.description !== todoDescription?.description) {
            try {
                if (todoDescription) {
                    const request =
                        {
                            id: todoDescription.id,
                            todoItemId: todoItem?.id,
                            description: todoDescriptionFormState.description,
                            url: todoDescriptionFormState.url,
                            createdAt: todoDescription.createdAt,
                            updatedAt: new Date(),
                        }
                    const updateTodoDescription = await editTodoDescriptionAPI(request);

                    dispatch(editTodoDescription({
                        todoItemId: todoItem?.id!, changedValues: {
                            ...request,
                            updatedAt: request.updatedAt,
                        }
                    })) //update state

                } else {
                    const newTodoDescription = await createTodoDescriptionAPI(
                        {
                            todoItemId: todoItem?.id,
                            description: todoDescriptionFormState.description,
                            url: todoDescriptionFormState.url,
                            createdAt: new Date(),
                            updatedAt: new Date(),
                        }
                    );

                    if ('data' in newTodoDescription) {
                        dispatch(addTodoDescription(newTodoDescription.data)) //update state
                    }
                }

                setAlertState({
                    show: true,
                    error: false,
                    message: "Todo description saved"
                })
            } catch (error) {
                console.error("error on submit", error);
                setAlertState({
                    show: true,
                    error: true,
                    message: "Error on saving todo description"
                })
            }
        }
        if (new Date(todoDescriptionFormState.dueDate) !== todoItem?.dueDate || todoDescriptionFormState.priority !== todoItem.priority) {
            try {
                const body =
                    {
                        id: todoItem?.id,
                        title: todoItem?.title,
                        dueDate: new Date(todoDescriptionFormState.dueDate),
                        completed: todoItem?.completed,
                        priority: todoDescriptionFormState.priority,
                    }
                const updateTodo = await editTodoAPI(body);

                dispatch(editTodo({
                    id: todoItem?.id!,
                    changedValues: {
                        ...body,
                        dueDate: body.dueDate,
                    }
                }))

                setAlertState({
                    show: true,
                    error: false,
                    message: "Todo saved"
                })
            } catch (error) {
                console.error("error on submit", error);
                setAlertState({
                    show: true,
                    error: true,
                    message: "Error on saving todo"
                })
            }
        }


    }
    const isValidURL = (url : string) => {
        // Regular expression to validate URLs
        const urlRegex = /^(https?:\/\/)?(www\.)?[a-zA-Z0-9-]+(\.[a-zA-Z]{2,})(:\d{1,5})?([/\w-]*)*(\?\S*)?$/;
        return urlRegex.test(url);
    };

    const handleDelete = async () => {
        try {
            if (todoDescription) {
                console.log('here')
                await deleteTodoDescriptionAPI(todoDescription?.id);
                dispatch(deleteTodoDescription({todoItemId: todoItem?.id!}))
            }
            await deleteTodoAPI(todoItem?.id!);

            dispatch(deleteTodo({id: todoItem?.id!}));
            handleClose();
        } catch (error) {
            console.error("error on delete", error);
            setAlertState({
                show: true,
                error: true,
                message: "Error on deleting todo"
            })
        }
    }

    return (

        <Form onSubmit={onSubmit}>
            <Form.Group className="mb-3" controlId="formUrl">
                <Form.Label>Url</Form.Label>
                <Form.Control
                    required
                    isInvalid={!isValidURL(todoDescriptionFormState.url)}
                    type="text"
                    placeholder="url"
                    value={todoDescriptionFormState.url}
                    onChange={(event) => setTodoDescriptionFormState({
                        ...todoDescriptionFormState,
                        url: event.target.value
                    })}
                />
                <Form.Control.Feedback type="invalid" tooltip>
                    wrong format for url
                </Form.Control.Feedback>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                    as="textarea"
                    placeholder="Description"
                    style={{height: '100px'}}
                    value={todoDescriptionFormState.description}
                    onChange={(event) => setTodoDescriptionFormState({
                        ...todoDescriptionFormState,
                        description: event.target.value
                    })}
                />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formDueDate">
                <Form.Label>Due date</Form.Label>
                <Form.Control
                    type="date"
                    placeholder="due date"
                    value={todoDescriptionFormState.dueDate}
                    onChange={(event) => setTodoDescriptionFormState({
                        ...todoDescriptionFormState,
                        dueDate: event.target.value
                    })}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formPriority">
                <Form.Label>Priority</Form.Label>
                <Form.Select aria-label="Priority"
                             value={todoItem?.priority}
                             onChange={(event) => setTodoDescriptionFormState({
                                 ...todoDescriptionFormState,
                                 priority: Number(event.target.value)
                             })}>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                </Form.Select>
            </Form.Group>
            <Button variant="primary" type="submit" disabled={todoDescriptionFormState.description === "" ||
                todoDescriptionFormState.url === "" || !isValidURL(todoDescriptionFormState.url)}>
                Save
            </Button>
            <Button variant="danger" className="ms-2" onClick={handleDelete}>Delete</Button>
            <Alert variant="sucess" hidden={!alertState.show || !alertState.error}>
                {alertState.message}
            </Alert>
        </Form>

    );
}