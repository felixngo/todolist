import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {editTodo} from "../../redux/slices/todoSlice";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSquare, faSquareCheck} from "@fortawesome/free-regular-svg-icons";
import {useEditTodoAPIMutation} from "../../redux/slices/apiSlice";
import {Stack} from "react-bootstrap";

interface TaskFormProps {
    id: number;
    title: string;
    completed: boolean;
    priority: number;
    dueDate: string;
}


export default function CompletedTaskForm({id, title, completed, priority, dueDate}: TaskFormProps) {

    const [todoCheck, setTodoCheck] = useState<boolean>(completed);
    const dispatch = useDispatch();

    const [editTodoAPI] = useEditTodoAPIMutation();


    const onChange = async () => {
        setTodoCheck(!todoCheck)

        try {
            const body = {
                id: id,
                title: title,
                completed: !todoCheck,
                priority: priority,
                dueDate: new Date(dueDate),
            }
            const updateTodo = await editTodoAPI(body);
            dispatch(editTodo({id: id, changedValues: body}))
        } catch (err) {
            console.error("Failed to edit todo: ", err);
        }
    }
    return (
        <Stack direction="horizontal" gap={2}>
            <form>
                <i className="btn" data-toggle="tooltip" data-placement="bottom" hidden={todoCheck} onClick={onChange}>
                    <FontAwesomeIcon icon={faSquare} size="2xl"/>
                </i>
                <i className="btn" data-toggle="tooltip" data-placement="bottom" hidden={!todoCheck} onClick={onChange}>
                    <FontAwesomeIcon icon={faSquareCheck} size="2xl"/>
                </i>
            </form>
            <h4>
                {title}
            </h4>
        </Stack>
    )
}