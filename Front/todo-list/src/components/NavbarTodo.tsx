import React from "react";

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import {faCircleCheck} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export default function NavbarTodo() {
    return (
        <Navbar style={{backgroundColor: "#749BC2"}}>
            <Container>
                <Navbar.Brand href="" className="d-flex align-items-center">
                    <FontAwesomeIcon icon={faCircleCheck} size="2xl"/>
                    <h1 className="ms-2">Todoos</h1>
                </Navbar.Brand>
            </Container>
        </Navbar>
    );
}