import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {addTodo} from "../redux/slices/todoSlice";
import {useAppSelector} from "../redux/hooks";
import {useCreateTodoAPIMutation} from "../redux/slices/apiSlice";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSquarePlus} from "@fortawesome/free-solid-svg-icons";
import Button from "react-bootstrap/Button";

interface createTodoFormState {
    title: string;
    priority: number;
    dueDate: string;
}

interface alertState {
    show: boolean;
    error: boolean;
}
export default function CreateTodoForm() {
    const [state, setState] = useState<createTodoFormState>({
        title: "",
        priority: 1,
        dueDate: new Date().toString(),
    });

    const [alertState, setAlertState] = useState<alertState>({
        show: false,
        error: false,
    })


    const updateState = (newState : Partial<createTodoFormState>) => {
        setState((prevState) => ({
            ...prevState,
            ...newState,
        }));
    }

    const dispatch = useDispatch();

    const [createTodoAPI] = useCreateTodoAPIMutation();

    const todolist = useAppSelector((state) => state.todos);
    const onSubmit = async (event : any) => {
        event.preventDefault();
        try {
            const newTodo = await createTodoAPI(
                {
                    title: state.title,
                    completed: false,
                    priority: state.priority,
                    dueDate: new Date(state.dueDate),
                }
            );

            if ('data' in newTodo) {
                dispatch(addTodo(newTodo.data)) //update state
            }


            // Handle success, e.g., reset the form or update the UI
            setAlertState({
                show: true,
                error: false,
            })
            setState({
                title: "",
                priority: 1,
                dueDate: new Date().toString(),
            });

        } catch (error) {
            // Handle errors, e.g., display an error message
            setAlertState({
                show: true,
                error: true,
            })
            console.error("Error adding todo:", error);
        }
    }


    return (

        <div className="row m-1 p-3">
            <div className="alert alert-success" role="alert" hidden={!alertState.show || alertState.error}>
                New task added successfully
            </div>
            <div className="alert alert-danger" role="alert" hidden={!alertState.show || !alertState.error}>
                Error encountered while adding new task !
            </div>

            <div className="col col-11 mx-auto">
                <form onSubmit={onSubmit}>
                <div className="row bg-white rounded shadow-sm p-2 add-todo-wrapper align-items-center justify-content-center">
                    <div className="col">
                        <input
                            className="form-control form-control-lg border-0 add-todo-input bg-transparent rounded"
                            type="text"
                            placeholder="Add new task"
                            value={state.title}
                            onChange={(event) => updateState({ title: event.target.value })}
                        />
                    </div>
                    <div className="col-auto m-0 px-2 d-flex align-items-center">
                        <div className="input-group">
                            <span className="input-group-text">Priority</span>
                            <select id="disabledSelect" className="form-select" onChange={(event) => updateState({priority: Number(event.target.value)})} >
                                <option selected value={1}>1</option>
                                <option value={2}>2</option>
                                <option value={3}>3</option>
                            </select>
                        </div>
                    </div>
                    <div className="col-auto m-0 px-2 d-flex align-items-center">
                        <input
                            className={"form-control form-control-lg border-0 add-todo-input bg-transparent rounded"}
                            type="date"
                            placeholder="due date"
                            value={state.dueDate}
                            onChange={(event) => updateState({dueDate: event.target.value})}
                        />
                    </div>
                    <div className="col-auto px-0 mx-0 mr-2">
                        <Button type="submit" variant="link" disabled={state.title === ""}>
                            <FontAwesomeIcon icon={faSquarePlus} size="2xl"/>
                        </Button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    )
}