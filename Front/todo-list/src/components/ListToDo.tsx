import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import CompletedTaskForm from "./taskforms/completedTaskForm";
import {useFetchTodosAPIQuery, useFetchTodosDescriptionsAPIQuery} from "../redux/slices/apiSlice";
import {setTodos} from "../redux/slices/todoSlice";
import {faHourglassHalf, faPenToSquare} from "@fortawesome/free-regular-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {TodoItem} from "../redux/models/api/todoItem";
import ModalTaskDescription from "./ModalTaskDescription";
import {setTodosDescriptions} from "../redux/slices/todoDescriptionSlice";
import {TodoDescription} from "../redux/models/api/todoDescription";
import {Badge, ListGroup} from "react-bootstrap";
import {faArrowUp} from "@fortawesome/free-solid-svg-icons";

interface modalState {
    show: boolean;
    item: TodoItem | null;
    itemDescription: TodoDescription | null;
}

function formatDate(date: Date){
    const day = date.getDate();
    const monthNames = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];
    const month = monthNames[date.getMonth()];
    const year = date.getFullYear();
    const suffix = getDaySuffix(day);

    return `${day}${suffix} ${month} ${year}`;
}

function getDaySuffix(day : number){
    if (day >= 11 && day <= 13) {
        return 'th';
    }
    switch (day % 10) {
        case 1:
            return 'st';
        case 2:
            return 'nd';
        case 3:
            return 'rd';
        default:
            return 'th';
    }
}

export default function ListToDo() {
    const stateData = useSelector((state : any) => state.todos)
    const stateDataDescriptions = useSelector((state : any) => state.todosDescriptions)

    const dispatch = useDispatch();

    const {data: todosData = [], isFetching: isFetchingTodos} = useFetchTodosAPIQuery();
    const {data: descriptionsData = [], isFetching: isFetchingDescriptions} = useFetchTodosDescriptionsAPIQuery();


    useEffect(() => {
        if (!isFetchingTodos)
            dispatch(setTodos({todos: todosData}));
        if (!isFetchingDescriptions)
            dispatch(setTodosDescriptions({todosDescriptions: descriptionsData}));
    }, [isFetchingTodos, isFetchingDescriptions])

    const [modalState, setModalState] = useState<modalState>({
        show: false,
        item: null,
        itemDescription: null
    });

    const sortItems = (todoItems : TodoItem[]) => {
        return todoItems.sort((a, b) =>
            a.priority - b.priority
        )
    }

    const handleClose = () => setModalState({show: false, item: null, itemDescription: null});

    return (
        <>
            <div className="row mx-1 px-5 pb-3 w-80">
                <div className="col-md-10 mx-auto">
                    <ListGroup>
                        {sortItems(Object.values<TodoItem>(stateData)).map((item: TodoItem) => (
                            <ListGroup.Item key={item.id.toString()}
                                            className="d-flex align-items-center justify-content-start">
                                <div className="flex-grow-1">
                                    <CompletedTaskForm id={item.id} title={item.title} completed={item.completed}
                                                       dueDate={item.dueDate.toString()}
                                                       priority={item.priority}/>
                                </div>
                                <div
                                    className="d-flex justify-content-between align-items-start"> {/* Add some margin to separate the elements */}
                                    <h4>
                                        <Badge bg="warning">
                                            <FontAwesomeIcon
                                                icon={faHourglassHalf}/> {formatDate(new Date(item.dueDate))}
                                        </Badge>
                                        <Badge bg="secondary" className="ms-4">
                                            <FontAwesomeIcon icon={faArrowUp}/> {item.priority}
                                        </Badge>
                                    </h4>
                                    <i className="btn d-flex align-items-center" onClick={() => setModalState({
                                        show: true,
                                        item: item,
                                        itemDescription: stateDataDescriptions[item.id]
                                    })}>
                                        <FontAwesomeIcon icon={faPenToSquare} size="xl"/>
                                    </i>
                                </div>
                            </ListGroup.Item>
                        ))}
                    </ListGroup>
                </div>
            </div>
            <ModalTaskDescription show={modalState.show} item={modalState.item}
                                  itemDescription={modalState.itemDescription} handleClose={handleClose}/>
        </>
    )
}