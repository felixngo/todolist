import React from "react";
import Modal from "react-bootstrap/Modal";
import TodoDescriptionForm from "./taskforms/todoDescriptionForm";
import {TodoItem} from "../redux/models/api/todoItem";
import {faCircleCheck, faPen} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import {editTodo} from "../redux/slices/todoSlice";
import {useDispatch} from "react-redux";
import {useEditTodoAPIMutation} from "../redux/slices/apiSlice";
import {TodoDescription} from "../redux/models/api/todoDescription";

interface ModalTaskDescriptionProps {
    show: boolean;
    item: TodoItem | null;
    itemDescription: TodoDescription | null;
    handleClose: () => void;
}

export default function ModalTaskDescription({show, item, itemDescription, handleClose}: ModalTaskDescriptionProps) {

    const [editMode, setEditMode] = React.useState<boolean>(false);
    const [title, setTitle] = React.useState<string>(item?.title!);
    const dispatch = useDispatch();

    const [editTodoAPI, {isLoading}] = useEditTodoAPIMutation();

    const onSubmit = async () => {
        try {
            const body = {
                id: item?.id,
                title: title,
                completed: item?.completed,
                priority: item?.priority,
                dueDate: item?.dueDate
            }
            const updateTodo = await editTodoAPI(body);
            dispatch(editTodo({id: item?.id!, changedValues: body}))
            setEditMode(false);
        } catch (err) {
            console.error("Failed to edit todo: ", err);
        }

    }
    return (
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <div className="d-flex align-items-center">
                    <Modal.Title hidden={editMode}>{item?.title}</Modal.Title>
                    <i className="btn" onClick={() => setEditMode(true)} hidden={editMode}>
                        <FontAwesomeIcon icon={faPen}/>
                    </i>
                </div>

                <Form className="d-flex align-items-center" onSubmit={onSubmit}>
                    <Form.Control hidden={!editMode} type="text" placeholder="title" value={title}
                                  onChange={(event) => setTitle(event.target.value)}/>
                    <Button hidden={!editMode} variant="link" type="submit" className="ms-2">
                        <FontAwesomeIcon icon={faCircleCheck} size="xl"/>
                    </Button>
                </Form>


            </Modal.Header>
            <Modal.Body>
                <TodoDescriptionForm todoItem={item} todoDescription={itemDescription} handleClose={handleClose}/>
            </Modal.Body>
        </Modal>
    );
}