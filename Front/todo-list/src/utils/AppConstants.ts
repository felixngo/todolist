const AppConstants = {
    API_HOST: import.meta.env.VITE_API_HOST,
};

export default AppConstants;
