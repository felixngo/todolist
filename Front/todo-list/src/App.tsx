import "./App.css"
import CreateTodoForm from "./components/CreateTodoForm";
import ListToDo from "./components/ListToDo";
import NavbarTodo from "./components/NavbarTodo";

function App() {
    return (
        <div className="App">
            <NavbarTodo/>
            <CreateTodoForm/>
            <ListToDo/>
        </div>
    )
}

export default App
