import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {TodoDescription} from "../models/api/todoDescription";

interface TodoDescriptionState {
    [todoItemId: number]: TodoDescription;
}

const initialState: TodoDescriptionState = {};

const todoDescriptionSlice = createSlice({
    name: "todosDescriptions",
    initialState,
    reducers: {
        setTodosDescriptions: (state, action: PayloadAction<{ todosDescriptions: TodoDescription[] }>) => {
            // Use the correct type for 'state' and initialize it as an empty object {}
            return action.payload.todosDescriptions.reduce((acc, todoDescription) => {
                acc[todoDescription.todoItemId] = todoDescription;
                return acc;
            }, {} as TodoDescriptionState);
        },
        addTodoDescription: (state, action: PayloadAction<TodoDescription>) => {
            return {
                ...state,
                [action.payload.todoItemId]: action.payload
            }
        },
        editTodoDescription: (state, action: PayloadAction<{
            todoItemId: number,
            changedValues: Partial<TodoDescription>
        }>) => {
            return {
                ...state,
                [action.payload.todoItemId]: {
                    ...state[action.payload.todoItemId],
                    ...action.payload.changedValues
                }
            }
        },
        deleteTodoDescription: (state, action: PayloadAction<{ todoItemId: number }>) => {
            const newState = {...state};
            delete newState[action.payload.todoItemId];
            return newState;
        }
    }
});

export const {
    setTodosDescriptions,
    addTodoDescription,
    editTodoDescription,
    deleteTodoDescription
} = todoDescriptionSlice.actions;
export default todoDescriptionSlice.reducer;