import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {TodoItem} from "../models/api/todoItem";

// Define an interface for the state object
interface TodoState {
    [id: number]: TodoItem;
}

const initialState: TodoState = {};

const todoSlice = createSlice({
    name: "todos",
    initialState,
    reducers: {
        setTodos: (state, action: PayloadAction<{ todos: TodoItem[] }>) => {
            return action.payload.todos.reduce((acc, todo) => {
                acc[todo.id] = todo;
                return acc;
            }, {} as TodoState);
        },
        addTodo: (state, action: PayloadAction<TodoItem>) => {
            return {
                ...state,
                [action.payload.id]: action.payload
            }
        },
        editTodo: (state, action: PayloadAction<{ id: number, changedValues: Partial<TodoItem> }>) => {
            return {
                ...state,
                [action.payload.id]: {
                    ...state[action.payload.id],
                    ...action.payload.changedValues
                }
            }
        },
        deleteTodo: (state, action: PayloadAction<{ id: number }>) => {
            const newState = {...state};
            delete newState[action.payload.id];
            return newState;
        }
    }
});

export const {setTodos, addTodo, editTodo, deleteTodo} = todoSlice.actions;
export default todoSlice.reducer;