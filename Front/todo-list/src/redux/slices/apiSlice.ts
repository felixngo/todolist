import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import AppConstants from "../../utils/AppConstants";
import {TodoItem} from "../models/api/todoItem";
import {TodoDescription} from "../models/api/todoDescription";


export const apiSlice = createApi({
    reducerPath: 'todoAPI',
    baseQuery: fetchBaseQuery({
        baseUrl: `${AppConstants.API_HOST}/api`,
    }),
    endpoints(builder) {
        return {
            fetchTodosAPI: builder.query<TodoItem[], void>({
                query: () => ({
                    url: `/TodoItems`,
                    method: 'GET',
                }),
            }),
            createTodoAPI: builder.mutation<TodoItem, Partial<TodoItem>>({
                query: (body) => ({
                    url: `/TodoItems`,
                    method: 'POST',
                    body,
                })
            }),
            editTodoAPI: builder.mutation<{ message: string }, Partial<TodoItem>>({
                query: (body) => ({
                    url: `/TodoItems`,
                    method: 'PUT',
                    body,
                })
            }),
            getTodoByIdAPI: builder.query<TodoItem, number>({
                query: (id) => ({
                    url: `/TodoItems/${id}`,
                    method: 'GET',
                    id,
                })
            }),
            deleteTodoAPI: builder.mutation<void, number>({
                query: (id) => ({
                    url: `/TodoItems/${id}`,
                    method: 'DELETE',
                    id,
                })
            }),

            fetchTodosDescriptionsAPI: builder.query<TodoDescription[], void>({
                query: () => ({
                    url: `/TodoDescription`,
                    method: 'GET',
                }),
            }),
            createTodoDescriptionAPI: builder.mutation<TodoDescription, Partial<TodoDescription>>({
                query: (body) => ({
                    url: `/TodoDescription`,
                    method: 'POST',
                    body,
                })
            }),
            editTodoDescriptionAPI: builder.mutation<{ message: string }, Partial<TodoDescription>>({
                query: (body) => ({
                    url: `/TodoDescription`,
                    method: 'PUT',
                    body,
                })
            }),
            getTodoByIdDescriptionAPI: builder.query<TodoDescription, number>({
                query: (id) => ({
                    url: `/TodoDescription/${id}`,
                    method: 'GET',
                    id,
                })
            }),
            deleteTodoDescriptionAPI: builder.mutation<void, number>({
                query: (id) => ({
                    url: `/TodoDescription/${id}`,
                    method: 'DELETE',
                    id,
                })
            })
        };
    },
});

export const {
    useFetchTodosAPIQuery,
    useCreateTodoAPIMutation,
    useEditTodoAPIMutation,
    useGetTodoByIdAPIQuery,
    useDeleteTodoAPIMutation,
    useFetchTodosDescriptionsAPIQuery,
    useCreateTodoDescriptionAPIMutation,
    useEditTodoDescriptionAPIMutation,
    useGetTodoByIdDescriptionAPIQuery,
    useDeleteTodoDescriptionAPIMutation
} = apiSlice;

export default apiSlice.reducer;