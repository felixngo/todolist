export interface TodoItem {
    id: number;
    title: string;
    dueDate: Date;
    completed: boolean;
    priority: number;
}