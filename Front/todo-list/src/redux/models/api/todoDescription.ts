export interface TodoDescription {
    id: number;
    todoItemId: number;
    description: string;
    url: string;
    createdAt: Date;
    updatedAt: Date;
}