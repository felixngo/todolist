import {configureStore} from '@reduxjs/toolkit'
import todoReducer from './slices/todoSlice'
import todosDescriptionsReducer from './slices/todoDescriptionSlice'
import {apiSlice} from './slices/apiSlice'

export const store = configureStore({
    reducer: {
        todos: todoReducer,
        todosDescriptions: todosDescriptionsReducer,
        [apiSlice.reducerPath]: apiSlice.reducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(apiSlice.middleware)
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch